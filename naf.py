import lxml.etree
import datetime

XML_NAMESPACE = "http://www.w3.org/XML/1998/namespace"
XML = "{%s}" % XML_NAMESPACE
NSMAP = {'xml': XML_NAMESPACE}
TAG_LIST = ['NAF', 'nafHeader', 'fileDesc', 'public', 'raw']


# These are just used to populate make_NAF.
# Concievably we could just have a single dictionary since none of the keys
# are reused

default_headers = {'NAF': {'version': 'v3', XML+"lang": "en"},
                   'fileDesc': {},
                   'public': {}
                   }


def unflatten(data):
    output = dict(default_headers)
    for key in data:
        if key in ['title', 'author', 'creationtime', 'filename',
                   'filetype', 'pages']:
            output['fileDesc'][key] = data[key]
            continue
        if key in ['publicId', 'uri']:
            output['public'][key] = data[key]
            continue
        if key not in ['raw']:
            raise RuntimeError("Can't unflatten: found key %r" % key)
    return output


def make_NAF(data):
    """Create a NAF file given a dictionary of attributes for all tags"""
    tags = {}
    for tag in TAG_LIST:
        tags[tag] = lxml.etree.Element(tag, nsmap=NSMAP)

    # add structure
    tags['NAF'].append(tags['nafHeader'])
    tags['NAF'].append(tags['raw'])
    tags['nafHeader'].append(tags['fileDesc'])
    tags['nafHeader'].append(tags['public'])

    # add attributes
    all_headers = unflatten(data)
    for headertype in all_headers:
        this_header = all_headers[headertype]
        for header in this_header:
            if this_header[header] is not None:
                tags[headertype].attrib[header] = unicode(this_header[header])

    # add raw text
    #tags['raw'].text = lxml.etree.CDATA(data['raw'])
    tags['raw'].text = lxml.etree.CDATA(data['raw'])
    return lxml.etree.tostring(tags['NAF'], pretty_print=True, encoding='utf8')

#headers = {'raw': u'I am a \u2013 fish <><'}
#print make_NAF(headers)
