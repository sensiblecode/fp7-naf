import re
from dateutil.parser import parse

def plugin_example(doc):
    """
    All plugins take a Document object (doc).
    They then return a dictionary of text strings, with names as per NAF spec
    e.g. creationtime / author
    """
    return {"author": "ExampleAuthor",
            "creationtime": "2000-01-01 00:00:00"}  # check spec for format


def chronicle(doc):
    import lxml.html
    root = lxml.html.fromstring(doc.content)
    parsed = {}

    try:
        creationtime = root.xpath("//meta[@property='article:published_time']"
                                  "/@content")[0]
    except IndexError:
        creationtime = None
    if creationtime is not None:
        parsed['creationtime'] = creationtime

    try:
        author = ', '.join(root.xpath("//meta[@name='author']/@content"))
    except IndexError:
        author = None
    if author is not None:
        parsed['author'] = author
    return parsed


def BournemouthEcho(doc):
    import lxml.html
    from dateutil.parser import parse
    try:
        root = lxml.html.fromstring(doc.content)
    except :
        return {'author':'','creationtime':''}
    parsed = {}

    try:
        creationtime = root.xpath("//span[@class='articleDateTime']")[0].text
    except IndexError:
        creationtime = None
    if creationtime is not None:
        try:
            creationtime = creationtime.replace(' in', '')
            creationtime = creationtime.replace('Updated ', '')

            parsed['creationtime'] = parse(creationtime).isoformat(' ')
        except ValueError:
            creationtime = None

    try:
        author = root.xpath("//span[@class='articleAuthor']"
                            )[0].text_content().strip()
    except IndexError:
        author = None
    if author is not None:
        parsed['author'] = author.replace('By ', '')
    return parsed


def livecho(doc):
    from dateutil.parser import parse
    import lxml.html
    root = lxml.html.fromstring(doc.content)
    parsed = {}

    try:
        creationtime = root.xpath("string(//time/@datetime)")
    except IndexError:
        creationtime = None
    if creationtime is not None:
        try:
            parsed['creationtime'] = parse(creationtime).isoformat(' ')
        except ValueError:
            creationtime = None

    try:
        author = ', '.join(root.xpath("//meta[@name='author']/@content"))
    except IndexError:
        author = None
    if author is not None:
        parsed['author'] = author
    return parsed

def DorkingAndLeatherheadAdvertiser(doc):
    import lxml.html
    emailre = re.compile('(\S*\@\S*)')
    
    try:
        root = lxml.html.fromstring(doc.content)
    except :
        return {'author':'','creationtime':''}
    parsed = {}

    try:
        creationtime = root.xpath("//span[@class='publication-date']")[0].text
    except IndexError:
        creationtime = None
    if creationtime is not None:
        try:
            parsed['creationtime'] = parse(creationtime.replace('Posted: ', '')).isoformat(' ')
        except ValueError:
            creationtime = None

    try:
        author = root.xpath("//header[@class='editorial__header cf']/p")[0].text_content().strip()
    except IndexError:
        author = None
    if author is not None:
        print author
        m = emailre.search(author)
        if m is not None:
            author = author.replace(m.group(0),'').strip()
        parsed['author'] = author.replace('By ', '')
    return parsed
