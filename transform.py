"""Transform to NAF.

input: a scrapy jsonlines document with keys:
['response_url', 'content', 'parent_url',
'request_url', 'character_set', 'mime_type']

output: a directory structure of NAF files

Usage:
  transform.py [--url=<url>|<jsonlines>] [<plugin>] [--skip=<skip>]

Options:
  --skip=<skip>  Skip to this numbered line in file [default: 0].
  --url=<url>    Use this URL to test: send NAF to stdout.
"""

import sys
import json
import logging
import goose
from naf import make_NAF
import urlparse
import hashlib
# import readability  # removed from requirements!
import plugins
from docopt import docopt
sys.path.append("/home/incoming")
import demo
import scraperwiki
import requests

class MyGooseConfig(goose.configuration.Configuration):
    @property
    def local_storage_path(self):
        self.enable_image_fetching = False
        return './goosetmp'

g = goose.Goose(config=MyGooseConfig())


class TransformError(RuntimeError):
    pass


class UnsupportedMIMEType(TransformError):
    pass


class NotJSONError(RuntimeError):
    pass

class FakeArticle:
    title = ""
    publish_date = ""
    cleaned_text = ""
    def __init__(self):
        self.title = ""
        self.publish_date = ""
        self.cleaned_text = ""

class Document(object):
    """A webpage; a jsonlines row, a naf file.
    All these are representations of the underlying document.
    NAF interface:
    ['title', 'author', 'creationtime', 'filename', 'filetype',
     'pages', 'publicId', 'uri'] + 'raw'"""
    def _html_goose(self):
        """Attributes available:
           ['meta_description', 'domain', 'meta_keywords', 'title', 'doc',
            'link_hash', 'tags', 'meta_favicon', 'additional_data', 'top_node',
            'movies', 'raw_doc', 'final_url', 'publish_date', 'cleaned_text',
            'canonical_link', 'top_image', 'raw_html', 'meta_lang']"""
        if len(self.content) == 0:
            article = FakeArticle()
        else:
            article = g.extract(raw_html=self.content)
        return {'title': article.title,
                'creationtime': article.publish_date,  # TODO isoformat
                'raw': article.cleaned_text
                }

    def _html_readability(self):
		raise NotImplementedError
        # doc = readability.readability.Document(self.content)
        #return {'title': doc.short_title(),
        #        'raw': doc.summary()
        #        }

    def _text(self):
        return {'raw': self.content}

    def filename(self):
        return urlparse.urlparse(self.response_url).path.split('/')[-1]

    def __init__(self, jsonline, plugin=lambda x: {}):
        self.plugin = plugin
        handler_by_mime = {"text/html": self._html_goose,  # _readability,
                           "text/plain": self._text}

        try:
            line = json.loads(jsonline)
        except:
            raise NotJSONError(jsonline)
        for key, value in line.items():
            setattr(self, key, value)

        try:
            self.body_handler = handler_by_mime[self.mime_type]
        except:
            raise UnsupportedMIMEType(self.mime_type, self.request_url)

    def meta(self):
        filetype_by_mime = {"text/html": 'HTML',
                            "text/plain": 'TEXT'}
        return {'filetype': filetype_by_mime.get(self.mime_type),
                'filename': self.filename(),
                'uri': self.response_url,
                'publicId': hashlib.sha224(self.response_url).hexdigest()
                }

    def naf(self):
        nafdata = self.meta()
        nafdata.update(self.body_handler())
        plugin_response = self.plugin(self)
        if plugin_response:
            logging.info(plugin_response)
        nafdata.update(plugin_response)
        return make_NAF(nafdata)


def main():
    arguments = docopt(__doc__, version='NAF Transform 0.1')

    filename = arguments['<jsonlines>']

    if '<plugin>' not in arguments:
	plugin_string = filename.split('/')[-1].partition('.')[0]
    else:
	plugin_string = arguments['<plugin>']

    plugin = None
    for plugin_module in [plugins, demo]:
	try:
	    plugin = getattr(plugin_module, plugin_string)
	    logging.info("Using plugin {!r}".format(plugin_string))
	    break
	except:
	    continue
    if plugin == None:
	logging.warn("No plugin {!r}".format(plugin_string))
	plugin = lambda x: {}

    skip = int(arguments['--skip'])

    if not(arguments['--url']):
	with open(filename) as f:
	    for i, line in enumerate(f):
		if i < skip:
		    continue
		print i
		try:
		    with open(filename+"."+str(i)+".xml", "w") as w:
			w.write(Document(line, plugin).naf())
		except UnsupportedMIMEType, e:
		    logging.warn(repr(e))
    else:
        url = arguments['--url']
        c = requests.get(url).content
        line = json.dumps({'content': c,
                           'request_url': url,
                           'response_url': url,
                           'parent_url': url,
                           'mime_type': 'text/html',
                           'character_set': 'utf-8'})
        print Document(line, plugin).naf()

if __name__ == "__main__":
    scraperwiki.status("ok", "Please wait, converting to NAF")
    try:
        main()
    except Exception as e:
        scraperwiki.status("error", str(e))
    else:
        scraperwiki.status("ok", "Converted to NAF")
    
